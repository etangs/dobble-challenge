from PIL import Image, ImageDraw#, ImageFront
import os
class Symbol:
    def __init__(self,workdir="/tmp"):
        self.workdir=workdir
        self.height=25
        self.width=20

    def generate(self,n=25):
        if not os.path.isdir(self.workdir):
            os.makedirs(self.workdir,exist_ok=True)
        #generate n symbols
        for i in range(n):
            img=Image.new('RGB',(self.width,self.height),color='blue')
            draw=ImageDraw.Draw(img)
            text="%d"%(i)
            textWidth,textHeight=draw.textsize(text)
            x=int((self.width-textWidth)/2)
            y=int((self.height-textHeight)/2)
            draw.text((x,y),text,fill=(255,255,255))
            img.save(os.path.join(self.workdir,'symbol-%d.png'%(i)))

if __name__=='__main__':
    symbol=Symbol(workdir="symbols")
    symbol.generate()
