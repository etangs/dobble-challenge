"""OpenAPI v3 Specification"""

# apispec via OpenAPI
from apispec import APISpec
from apispec.ext.marshmallow import MarshmallowPlugin
from apispec_webframeworks.flask import FlaskPlugin
from marshmallow import Schema, fields

# Create an APISpec
spec = APISpec(
    title="Dobble Challenge Backend API",
    version="1.0.0",
    openapi_version="3.0.2",
    plugins=[FlaskPlugin(), MarshmallowPlugin()],
)

# Define schemas
class InputNewCards(Schema):
    #number = fields.Int(description="An integer.", required=True)
    url = fields.String(description="URL to Git Repository", required=True,example="https://github.com/EtangDesApplis/ReTeX.git")
    main = fields.String(description="Main tex file", required=True, example="test/cv_homer.tex")

class OutputNewCards(Schema):
    Status = fields.String(description="A message.", required=True)
    OutputFile = fields.String(description="A message.", required=True)
    LogFile = fields.String(description="A message.", required=True)

class InputNewGame(Schema):
    #number = fields.Int(description="An integer.", required=True)
    url = fields.String(description="URL to Git Repository", required=True,example="https://github.com/EtangDesApplis/ReTeX.git")
    main = fields.String(description="Main tex file", required=True, example="test/cv_homer.tex")

class OutputNewGame(Schema):
    Status = fields.String(description="A message.", required=True)
    OutputFile = fields.String(description="A message.", required=True)
    LogFile = fields.String(description="A message.", required=True)

class InputPlay(Schema):
    #number = fields.Int(description="An integer.", required=True)
    url = fields.String(description="URL to Git Repository", required=True,example="https://github.com/EtangDesApplis/ReTeX.git")
    main = fields.String(description="Main tex file", required=True, example="test/cv_homer.tex")

class OutputPlay(Schema):
    Status = fields.String(description="A message.", required=True)
    OutputFile = fields.String(description="A message.", required=True)
    LogFile = fields.String(description="A message.", required=True)

class InputUpdate(Schema):
    #number = fields.Int(description="An integer.", required=True)
    url = fields.String(description="URL to Git Repository", required=True,example="https://github.com/EtangDesApplis/ReTeX.git")
    main = fields.String(description="Main tex file", required=True, example="test/cv_homer.tex")

class OutputUpdate(Schema):
    Status = fields.String(description="A message.", required=True)
    OutputFile = fields.String(description="A message.", required=True)
    LogFile = fields.String(description="A message.", required=True)

class OutputHealthz(Schema):
    Status = fields.String(description="A message.", required=True)

# register schemas with spec
#spec.components.schema("Input", schema=InputSchema)
#spec.components.schema("Output", schema=OutputSchema)

# add swagger tags that are used for endpoint annotation
tags = [
            {'name': 'Inner Endpoints',
             'description': 'To debug'
            },
            {'name': 'Outer Endpoints',
             'description': 'To expose'
            },
       ]

for tag in tags:
    spec.tag(tag)