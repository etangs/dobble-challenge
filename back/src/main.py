from flask import Flask, request
from flask_cors import CORS
import time, os, shutil, glob, logging, socket
from subprocess import Popen, PIPE
from flask import send_from_directory

from cards import *
from flask_pymongo import PyMongo

#swagger
from flask import jsonify

app = Flask(__name__)
app.config["MONGO_URI"]="mongodb://%s/dobble-challenge"%(os.getenv("MONGO_SVC"))
CORS(app)
mongo=PyMongo(app)

rootDir=os.getenv("ROOT_DIR","/tmp")

#serve card image: http://localhost:5000/1631309418258/card-53.png
@app.route('/<cardsId>/<cardName>', methods=['GET'])
def download_image(cardsId,cardName):
    return send_from_directory(os.path.join(rootDir,"cards",cardsId),cardName)

@app.route('/healthz', methods=['GET'])
def healthz():
  """
    ---
    get:
      description: API health check
      responses:
        '200':
          description: call successful
          content:
            application/json:
              schema: OutputHealthz
      tags:
          - Inner Endpoints
  """
  hostname=socket.gethostname()
  return {"Status":socket.gethostbyname(hostname)},200

@app.route('/game', methods=['GET'])
def new_game():
    """
    ---
    get:
      description: Create a new game
      requestBody:
        required: true
        content:
            application/json:
                schema: InputNewGame
      responses:
        '201':
          description: call successful
          content:
            application/json:
              schema: OutputNewGame
      tags:
          - Outer Endpoints
    """
    return {"Status":"OK"},200

@app.route('/game/<guid>', methods=['POST'])
def play_game(guid):
    """
    ---
    post:
      description: Play a game
      parameters:
      - name: guid
        in: path
        type: string
        required: true
        default: all
      requestBody:
        required: true
        content:
            application/json:
                schema: InputNewGame
      responses:
        '201':
          description: call successful
          content:
            application/json:
              schema: OutputNewGame
      tags:
          - Outer Endpoints
    """
    return {"Status":"OK"},200

@app.route('/cards', methods=['POST','GET'])
def manage_cards():
    """
    ---
    post:
      description: Create new set of cards providing theme
      requestBody:
        required: true
        content:
            application/json:
                schema: InputNewCards
      responses:
        '201':
          description: call successful
          content:
            application/json:
              schema: OutputNewCards
      tags:
          - Inner Endpoints
    get:
      description: Show the cards of current set
      requestBody:
        required: true
        content:
            application/json:
                schema: InputNewCards
      responses:
        '201':
          description: call successful
          content:
            application/json:
              schema: OutputNewCards
      tags:
          - Inner Endpoints
    """
    try:
        if request.method=='POST':
            collection=None
            try:
                payload=request.get_json()
                if os.path.isdir(os.path.join(rootDir,"symbols",payload["theme"])):
                    collection=os.path.join(rootDir,"symbols",payload["theme"])
                    logging.info("... generating cards from theme: %s"%(payload["theme"]))
            except:
                logging.info("... no theme is provided")
                # random a collection from /symbols/<collection>
                collections=glob.glob(os.path.join(rootDir,"symbols","*"))
                if len(collections)>0:
                    collection=random.choice(collections)

            # create set of cards in /cards/<epoch>
            cardsId=str(int(time.time()*1000))
            cards=Cards(cardDir=os.path.join(rootDir,"cards",cardsId),symbolDir=collection)
            
            # write set of cards in mongodb
            mongo.db.cards.insert_one({"id": cardsId,"data": cards.cards})
            # set to default card set
            state=mongo.db.state.find_one()
            if current==None:
                #first run
                mongo.db.state.insert_one({"cardsId":cardsId})
            else:
                mongo.db.state.update_one({},{"$set":{"cardsId":cardsId}})
            
            """
            {
                "currentCards":16800000,
                "bestOfDay": {"name":"player1","message":"love it","score":112.345},
                "bestOfMonth": {"name":"player2","message":"love it","score":11.345},
                "bestOfAll": {"name":"player3","message":"love it","score":10.345},
            }
            """

            return {"Status":cardsId},201
        else:
            #GET request
            # get the current cardsId
            state=mongo.db.state.find_one()
            cards=mongo.db.cards.find_one({"id":state["cardsId"]})
            return cards,200

    except Exception as error:
        logging.error("Encounter error: %s"%(str(error)))
        return {"Status":str(error)},500

#/play
#/update

#swagger
from api_spec import spec
from swagger import swagger_ui_blueprint, SWAGGER_URL
with app.test_request_context():
    spec.path(view=app.view_functions["healthz"])
    spec.path(view=app.view_functions["new_game"])
    spec.path(view=app.view_functions["manage_cards"])
    spec.path(view=app.view_functions["play_game"])
@app.route("/api/swagger.json")
def create_swagger_spec():
    """
    Swagger API definition.
    """
    return jsonify(spec.to_dict())

app.register_blueprint(swagger_ui_blueprint, url_prefix=SWAGGER_URL)

if __name__=="__main__":
    app.run(host='0.0.0.0')