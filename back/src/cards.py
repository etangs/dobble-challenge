import os, glob, logging, math
from symbols import *
import random
from PIL import Image
class Card:
    def __init__(self,workdir="/tmp"):
        self.workdir=workdir
        self.a=500
        #radius of rotation
        self.r=int(self.a/3.)
        self.lmax=int(self.r*4/5)
        self.lmin=int(self.r/3.)
        self.background=(255, 255, 255)

    def generate(self,cardName,symbols):

        n=len(symbols)-1
        azimuths=range(n)
        #random the azimuth position
        theta0=random.uniform(0,2*math.pi)
        #random scale

        imgBase=Image.new('RGB',(self.a,self.a),color='white')
        for i in range(len(symbols)-1):
            img=Image.open(symbols[i])
            #randomly rotate
            rotatedImg=img.rotate(random.uniform(0,360),fillcolor=self.background,resample=Image.BICUBIC, expand = True)
            dx,dy=rotatedImg.size
            #randomly scale
            scale=random.uniform(self.lmin,self.lmax)/float((max(dx,dy)))
            scaledImg=rotatedImg.resize((int(scale*dx),int(scale*dy)))
            # define azimuth position
            theta=azimuths[i]*(2*math.pi/n)+theta0
            x=int(math.sin(theta)*self.r+self.a/2.-scale*dx/2.)
            y=int(math.cos(theta)*self.r+self.a/2.-scale*dy/2.)
            imgBase.paste(scaledImg,(x,y))
        #add center image
        img=Image.open(symbols[-1])
        #randomly rotate
        rotatedImg=img.rotate(random.uniform(0,360),fillcolor=self.background, resample=Image.BICUBIC, expand = True)
        dx,dy=img.size
        #randomly scale
        scale=random.uniform(self.lmin,self.lmax)/float((max(dx,dy)))
        scaledImg=rotatedImg.resize((int(scale*dx),int(scale*dy)))
        x=int(self.a/2.-scale*dx/2.)
        y=int(self.a/2.-scale*dy/2.)
        imgBase.paste(scaledImg,(x,y))
        imgBase.save(os.path.join(self.workdir,cardName))

class Cards:
    def __init__(self,cardDir="/tmp",symbolDir=None):
        self.cardDir=cardDir
        self.symbolDir=symbolDir
        self.cards={}

    def spotIt(self,cardId1,cardId2):
        res=None
        for sym1 in self.cards[cardId1]:
            for sym2 in self.cards[cardId2]:
                if sym1==sym2:
                    res=sym1
                    break
            if res != None:
                break
        logging.info("Card %d and card %d has in common %s"%(cardId1,cardId2,res))

    def generate(self,n=7):
        # nb of symbols
        nbSymbols=n**2+n+1
        # nb of symbols per card
        nbSymbolsPerCard=n+1
        if not os.path.isdir(self.cardDir):
            os.makedirs(self.cardDir,exist_ok=True)
        # if not enough symbols given, we ignore

        if self.symbolDir==None:
            self.symbolDir=os.path.join(self.cardDir,"symbols")
            symbol=Symbol(self.symbolDir)
            symbol.generate(nbSymbols)
        
        # algo: https://www.101computing.net/the-dobble-algorithm/
        symbols=[]
        cards=[]
        for img in glob.glob(os.path.join(self.symbolDir,"*.*")):
            symbols.append(img)
        logging.debug(symbols)
        #add first set  of n+1 cards
        for i in range(n+1):
            #add new card with first symbol
            cards.append([1])
            #add n+1 symbols on the card
            for j in range(n):
                cards[i].append((j+1)+(i*n)+1)
        #add n set of n cards
        for i in range(0,n):
            for j in range(0,n):
                #append a new card with 1 symbol
                cards.append([i+2])
                # add n symbol on the card
                for k in range(0,n):
                    val=(n+1+n*k+(i*k+j)%n)+1
                    cards[len(cards)-1].append(val)
        for card in cards:
            random.shuffle(card)
        #generate cards
        i=0
        cardGenerator=Card(self.cardDir)
        for card in cards:
            self.cards[i]=[]
            for index in card:
                self.cards[i].append(symbols[index-1])
            cardGenerator.generate("card-%d.png"%(i),self.cards[i])
            i=i+1
        logging.debug(self.cards)
        return self.cards

if __name__=="__main__":
    logging.basicConfig(
        format='%(asctime)s %(levelname)-8s %(message)s',
        level=logging.DEBUG,
        datefmt='%Y-%m-%d %H:%M:%S')
    cards=Cards(cardDir="cards")
    cards.generate()
    cards.spotIt(0,1)
